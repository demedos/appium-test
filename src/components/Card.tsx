import React from 'react';
import { View, Text } from 'react-native';

const shadow = {
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.2,
  shadowRadius: 1.41,

  elevation: 2,
};

const Card = ({ index }: { index: number }) => {
  return (
    <View
      key={index}
      style={{
        padding: 20,
        backgroundColor: 'white',
        width: 300,
        borderRadius: 8,
        margin: 10,
        marginTop: index === 0 ? 20 : 10,
        ...shadow,
      }}>
      <Text>Home Screen {`${index + 1}`}</Text>
    </View>
  );
};

export default Card;
