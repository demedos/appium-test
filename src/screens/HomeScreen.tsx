import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import Card from '../components/Card';

const HomeScreen = () => {
  return (
    <SafeAreaView>
      <ScrollView>
        <View
          style={{
            alignItems: 'center',
          }}>
          {Array.from({ length: 50 }, (_, i) => (
            <Card key={i} index={i} />
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
